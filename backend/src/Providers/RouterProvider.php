<?php
declare(strict_types=1);

/**
 * This file is part of the Vökuró.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Vokuro\Providers;

use Exception;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Mvc\Router;
use Vokuro\Application;
use Vokuro\Plugins\JsonRPCRouter;

class RouterProvider implements ServiceProviderInterface
{
    /**
     * @var string
     */
    protected $providerName = 'router';

    /**
     * @param DiInterface $di
     *
     * @return void
     */
    public function register(DiInterface $di): void
    {
        /** @var Application $application */
        $application = $di->getShared(Application::APPLICATION_PROVIDER);
        $dipatcher = $di->getShared('dispatcher');
        /** @var string $basePath */
        $basePath = $application->getRootPath();

        $di->set($this->providerName, function () use ($basePath, $dipatcher) {
            $router = new Router();
            $data = \GuzzleHttp\json_decode(file_get_contents('php://input'), true);

            if (!isset($data['jsonrpc'])) {
                throw new Exception("The request is not Json-RPC");
            }

            $method = explode('.', $data['method']);

            $controller = $method[0];
            $action = $method[1];
            $params = $data['params'];

            $dipatcher->setControllerName($controller);
            $dipatcher->setActionName($action);
            $dipatcher->setParams($params);

            return $router;
        });
    }
}
