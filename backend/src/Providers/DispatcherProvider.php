<?php


namespace Vokuro\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Vokuro\Plugins\JsonRpcDispatcher;

class DispatcherProvider implements ServiceProviderInterface
{
    /**
     * @var string
     */
    protected $providerName = 'dispatcher';

    /**
     * @param DiInterface $di
     *
     * @return void
     */
    public function register(DiInterface $di): void
    {
        $di->set($this->providerName, function () {
            $dispatcher = new JsonRpcDispatcher();
            $dispatcher->setDefaultNamespace('Vokuro\Controllers');

            return $dispatcher;
        });
    }
}
