<?php
declare(strict_types=1);

/**
 * This file is part of the Vökuró.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Vokuro\Models;

use Phalcon\Mvc\Model;


class Url extends Model
{
    /**
     *
     */
    public function initialize()
    {
        $this->setSource('urls');
    }
    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $slug;
    /**
     * @var string
     */
    public $url;
    /**
     * @var string
     */
    public $created_at;
    /**
     * @var string
     */
    public $updated_at;
}
