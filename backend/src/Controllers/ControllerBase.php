<?php
declare(strict_types=1);

/**
 * This file is part of the Vökuró.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Vokuro\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

/**
 * ControllerBase
 * This is the base controller for all controllers in the application
 *
 */
class ControllerBase extends Controller
{

}
