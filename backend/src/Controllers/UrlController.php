<?php
declare(strict_types=1);

namespace Vokuro\Controllers;

use Phalcon\Validation;
use Vokuro\Exceptions\Exception;
use Vokuro\Models\Url;


class UrlController extends ControllerBase
{
    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function initialize(): void
    {
        $this->view->setTemplateBefore('public');
    }

    /**
     * @throws Exception
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            throw new Exception(
                Exception::ERROR_METHOD_NOT_FOUND['message'],
                Exception::ERROR_METHOD_NOT_FOUND['code']
            );
        }

        $params = $this->dispatcher->getParams();

        $validation = new Validation();
        $validation->add(
            'url',
            new Validation\Validator\PresenceOf(
                [
                    'message' => 'The url is required',
                ]
            )
        );
        $validation->add(
            'url',
            new Validation\Validator\Url([
                'message' => 'The url is not valid'
            ])
        );
        $messages = $validation->validate($params);

        if (!count($messages)) {
            $url = $params['url'];
            if (Url::findFirst(["url = '$url'"])) {
                throw new Exception(
                    Exception::ERROR_URL_ALREADY_EXIST['message'],
                    Exception::ERROR_URL_ALREADY_EXIST['code']
                );
            }

            do {
                $slug = substr(md5(uniqid((string) mt_rand(), true)), 0, 4);
            } while (Url::findFirst(["slug = '$slug'"]));

            $url = new Url([
                'slug' => $slug,
                'url'  => $url
            ]);

            if (!$url->save()) {
                throw new Exception(
                    Exception::ERROR_VALIDATION['message'],
                    Exception::ERROR_VALIDATION['code']
                );
            }

            return $url->toArray();
        } else {
            throw new Exception(
                Exception::ERROR_VALIDATION['message'],
                Exception::ERROR_VALIDATION['code']
            );
        }
    }

    /**
     * @throws Exception
     */
    public function getAction()
    {
        if (!$this->request->isPost()) {
            throw new Exception(
                Exception::ERROR_METHOD_NOT_FOUND['message'],
                Exception::ERROR_METHOD_NOT_FOUND['code']
            );
        }

        $params = $this->dispatcher->getParams();

        if (!isset($params['slug'])) {
            throw new Exception(
                Exception::ERROR_METHOD_NOT_FOUND['message'],
                Exception::ERROR_METHOD_NOT_FOUND['code']
            );
        }

        $validation = new Validation();
        $validation->add(
            'slug',
            new Validation\Validator\PresenceOf(
                [
                    'message' => 'The slug is required',
                ]
            )
        );

        $messages = $validation->validate($params);
        $slug = $params['slug'];

        if (!count($messages)) {
            $url = Url::findFirst(["slug = '$slug'"]);

            if (!$url) {
                throw new Exception(
                    Exception::ERROR_URL_NOT_FOUND['message'],
                    Exception::ERROR_URL_NOT_FOUND['code']
                );
            }
        } else {
            throw new Exception(
                Exception::ERROR_VALIDATION['message'],
                Exception::ERROR_VALIDATION['code']
            );
        }

        return $url->toArray();
    }
}
