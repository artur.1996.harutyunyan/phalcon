<?php
declare(strict_types=1);

namespace Vokuro\Migrations;

use Phinx\Migration\AbstractMigration;

final class CreateUrls extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('urls');
        if ($table->exists()) {
            return;
        }

        $table->addColumn('slug', 'char', ['limit' => 255])
            ->addColumn('url', 'char', ['limit' => 255])
            ->addTimestamps()
            ->create();
    }
}
