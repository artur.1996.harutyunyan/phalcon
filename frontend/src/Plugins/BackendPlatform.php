<?php


namespace Vokuro\Plugins;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Phalcon\Config;
use GuzzleHttp\Client;


use Vokuro\Exceptions\ApiErrorException;
use Vokuro\Models\Url;


class BackendPlatform
{
    /**
     * @var Config
     */
    protected $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }


    /**
     * @param $url
     * @return Url
     * @throws ApiErrorException
     */
    public function createShortUrl($url)
    {
        /**
         * @var Client $client
         * @var Request $request
         */
        $client = new Client();
        $body = [
            'jsonrpc' => '2.0',
            'method'  => 'url.create',
            'params'  => [
                'url' => $url
            ],
            'id' => uniqid()
        ];
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ];

        try {
            $response =  $client->request('POST', $this->config->path('application.apiUrl'), [
                'headers' => $headers,
                'json' => $body
            ]);
        } catch (RequestException $exception) {
            throw new ApiErrorException($exception->getMessage(), $exception->getCode());
        }

        $body = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

        if (isset($body['error'])) {
            throw new ApiErrorException($body['error']['message'],
                $body['error']['code']
            );
        }

        return new Url($body['result']);
    }

    /**
     * @param $slug
     * @return Url
     * @throws ApiErrorException
     */
    public function getShortUrlBySlug($slug)
    {
        /**
         * @var Client $client
         * @var Request $request
         */
        $client = new Client();
        $body = [
            'jsonrpc' => '2.0',
            'method'  => 'url.get',
            'params'  => [
                'slug' => $slug
            ],
            'id' => uniqid()
        ];
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ];

        try {
            $response =  $client->request('POST', $this->config->path('application.apiUrl'), [
                'headers' => $headers,
                'json' => $body
            ]);
        } catch (RequestException $exception) {
            throw new ApiErrorException($exception->getMessage(), $exception->getCode());
        }

        $body = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

        if (isset($body['error'])) {
            throw new ApiErrorException($body['error']['message'],
                $body['error']['code']
            );
        }

        return new Url($body['result']);
    }
}
