<?php
declare(strict_types=1);

/**
 * This file is part of the Vökuró.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Vokuro\Models;


class Url
{
    /**
     * Url constructor.
     * @param array $attrs
     */
    public function __construct(array $attrs)
    {
        $this->id = $attrs['id'] ?? null;
        $this->slug = $attrs['slug'] ?? null;
        $this->url = $attrs['url'] ?? null;
        $this->created_at = $attrs['created_at'] ?? null;
        $this->updated_at = $attrs['updated_at'] ?? null;
    }

    /**
     * @var integer $id
     */
    public $id;
    /**
     * @var string $slug
     */
    public $slug;
    /**
     * @var string $url
     */
    public $url;
    /**
     * @var string $created_at
     */
    public $created_at;
    /**
     * @var $updated_at
     */
    public $updated_at;

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'url' => $this->url,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
