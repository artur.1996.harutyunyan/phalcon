<?php

namespace Vokuro\Exceptions;

use Phalcon\Exception;
use Throwable;

class ApiErrorException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
