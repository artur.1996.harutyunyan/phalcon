<?php
declare(strict_types=1);

/**
 * This file is part of the Vökuró.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Vokuro\Controllers;

use Vokuro\Exceptions\ApiErrorException;
use Vokuro\Plugins\BackendPlatform;
use Vokuro\Forms\UrlForm;

/**
 * Controller used handle non-authenticated session actions like login/logout,
 * user signup, and forgotten passwords
 */
class UrlController extends ControllerBase
{
    /**
     * @var BackendPlatform $backendPlatform
     */
    private $backendPlatform;
    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function initialize(): void
    {
        $this->view->setTemplateBefore('public');
        $this->backendPlatform = new BackendPlatform($this->config);
    }

    /**
     * Allow a user to signup to the system
     */
    public function indexAction()
    {
        $form = new UrlForm();
        $this->view->setVar('form', $form);

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                $url = $form->getValue('url');
                $createdSuccessfully = true;

                try {
                    $url = $this->backendPlatform->createShortUrl($url);
                } catch (ApiErrorException $exception) {
                    $this->flash->error($exception->getMessage());
                    $createdSuccessfully = false;
                }
                if ($createdSuccessfully) {
                    $pathOfGetUrl = $this->config->path('application.publicUrl') . '/url/' . $url->slug;
                    $this->flash->success(
                        (string) "The url has shorted successfully: Link is $pathOfGetUrl"
                    );
                }
            }
        }
    }

    /**
     * @param null $slug
     * @return \Phalcon\Http\ResponseInterface| void
     */
    public function getAction($slug)
    {
        if (!$this->request->isGet()) {
            return $this->notFound();
        }

        try {
            $url = $this->backendPlatform->getShortUrlBySlug($slug);
        } catch (ApiErrorException $e) {
            return $this->notFound();
        }

        return $this->response->redirect($url->url);
    }
}
