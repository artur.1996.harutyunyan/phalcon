{% set isUrlValidClass = form.messages('url') ? 'form-control is-invalid' : 'form-control' %}

<div class="container">
    {{ flash.output() }}
    <form method="post">
        <div class="form-group">
            <div class="col-sm-12">
               <label for="email">URl:</label>
                {{ form.render('url', ['class': isUrlValidClass, 'placeholder': 'Url']) }}
                <div class="invalid-feedback">
                    {{ form.messages('url') }}
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

