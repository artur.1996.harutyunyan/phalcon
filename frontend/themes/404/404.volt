<div align="center">
    <div class="sub-content">
        <strong>ERROR 404</strong>
        <br />
        <br />
        You have tried to access a page which does not exist or has been moved.
    </div>
</div>
